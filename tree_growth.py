'''
This file is part of FrontLine.

FrontLine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FrontLine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with FrontLine.  If not, see <https://www.gnu.org/licenses/>.
'''

import argparse
import os.path
import platform
import time
from win32_setctime import setctime
from datetime import datetime
from random import random
from numpy.random import choice
from numpy.random import randint
from collections import Counter
from shutil import copy

# Setup parameters (to be (partially) made user-input)
# Extensions based on popular file types as listed in CryptoLock (Scaife et. al)
EXTENSIONS = [".pdf", ".odt", ".docx", ".pptx", ".txt", ".mov", ".zip", ".pages", ".jpg", ".xls", ".csv", ".doc",
              ".ppt", ".gif", ".png", ".xml", ".html", ".xlsx", ".mp3", ".log", ".ogg", ".wav"]
TOP_LEVEL_DIRS = ["Desktop", "Documents", "Downloads", "Music", "Pictures", "Videos"]
SUB_DIR_NAMES = ["Work", "Holiday", "Timesheets", "Personal", "Games", "Memories", "Archive", "Favourites"]
TARGET_OS = "Windows"
DATE_LATEST = datetime.now()
DATE_EARLIEST = "2019-09-10 00:00:00"
DATE_FORMAT = "%Y-%m-%d %H:%M:%S"

# Important paths
PATH_FILE = os.path.dirname(os.path.realpath(__file__))     # Path of this file
PATH_REPOSITORY = os.path.join(PATH_FILE, "repository")             # Path of repository (files used to populate user space)
PATH_SPACE = os.path.join(PATH_FILE, "spaces")              # Path of user spaces


class UserSpace:
    def __init__(self, ext_count, cap, rec_depth):
        self.ext_count = ext_count
        self.cap = cap
        self.rec_depth = rec_depth

    def determine_counts(self, ext_count):
        """
        Randomly decide how many files of each chosen type to include
        :param ext_count: How many extensions to generate a count for
        :return: A list of file counts
        """
        return randint(low=1, high=self.cap+1, size=ext_count).tolist()


def file_population(new_path, space):
    file_names = []
    extensions = []
    file_pairs = []
    path, dirs, files = os.walk(PATH_REPOSITORY).__next__()
    file_names.extend(files)
    # List extensions in repository by type and quantity
    for fn in file_names:
        extensions.append(".%s" % fn.split('.')[1])
    extension_counts = Counter(extensions)

    # Intersection of repository extensions with extensions we care about (in EXTENSIONS)
    available_extensions = extension_counts.keys()
    crossover_extensions = [value for value in available_extensions if value in EXTENSIONS]

    # Get counts of each extension
    ext_counts = space.determine_counts(len(crossover_extensions))

    # List every possible output directory
    possible_dirs = []
    for path, dirs, files in os.walk(new_path):
        possible_dirs.append(path)

    possible_dirs = possible_dirs[1:]

    for i in range(len(crossover_extensions) - 1):
        file_pairs.append([crossover_extensions[i], ext_counts[i]])

    for file_type, amount in file_pairs:
        # Copy 'amount' of 'file_type' into 'new_path', randomly selecting 'tld' each time
        for i in range(amount):
            chosen_file = choice([value for value in file_names if file_type in value])
            src_path = os.path.join(PATH_REPOSITORY, chosen_file)
            dest_path = os.path.join(new_path, choice(possible_dirs), chosen_file)
            if not os.path.exists(dest_path):
                copy(src_path, dest_path)


def create_sub_dir(path, creation_probability, depth):
    if not os.path.isdir(path):
        chance = random()
        if chance < creation_probability:
            os.mkdir(path)
            path = os.path.join(path, choice(SUB_DIR_NAMES))
            create_sub_dir(path, creation_probability - 0.20, depth - 1)


def directory_setup(space):
    """
    Initialise User Space directories (i.e. create them, as well as their top-level and sub-directories)
    :param space: The User Space within which to create directories
    :return: Path of the current User Space
    """
    if not os.path.isdir(PATH_SPACE):
        os.mkdir(PATH_SPACE)
    path, dirs, files = os.walk(PATH_SPACE).__next__()
    new_path = os.path.join(PATH_SPACE, str(len(dirs)))
    os.mkdir(new_path)

    # Create top-level directories
    for tld in TOP_LEVEL_DIRS:
        os.mkdir(os.path.join(new_path, tld))

    # Create sub-directories
    path, dirs, files = os.walk(new_path).__next__()
    for d in dirs:
        no_of_sub = randint(0, 5)
        for i in range(no_of_sub):
            chosen_dir = choice(SUB_DIR_NAMES)
            current_path = os.path.join(new_path, d, chosen_dir)
            create_sub_dir(current_path, 1, space.rec_depth)

    return new_path


def gen_timestamp(date_earliest):
    # https://stackoverflow.com/questions/553303/generate-a-random-date-between-two-other-dates
    stime = time.mktime(time.strptime(date_earliest, DATE_FORMAT))
    etime = time.mktime(time.strptime(str(DATE_LATEST)[:-7], DATE_FORMAT))
    ptime = stime + random() * (etime - stime)
    return ptime


def file_times(path, space):
    for path, dirs, files in os.walk(path):
        if platform.system() == TARGET_OS:
            # Set create time, then set modification time to be after creation time
            for f in files:
                actual_path = os.path.join(path, f)
                t1 = gen_timestamp(DATE_EARLIEST)
                t2 = gen_timestamp(str(datetime.fromtimestamp(t1))[:-7])
                setctime(actual_path, t1)
                os.utime(actual_path, (t2, t2))


def main():
    """
    Obtain user parameters for the current User Space, then kick off the program
    :return: NULL
    """

    # Handle user input
    parser = argparse.ArgumentParser(description='parameters for user document space')
    parser.add_argument('--ext_count', type=int, help='extension count', default=10)
    parser.add_argument('--doc_cap', type=int, help='maximum no. of doc type', default=10)
    parser.add_argument('--rec_depth', type=int, help='maximum nested folder depth', default=3)
    args = parser.parse_args()

    # Create the User Space object
    us = UserSpace(args.ext_count, args.doc_cap, args.rec_depth)

    # Set up the directory layout for the current User Space
    new_path = directory_setup(us)

    # Populate directory layout with files from Govdocs
    file_population(new_path, us)

    # Modify file creation and modification times
    file_times(new_path, us)


# Program start
main()
