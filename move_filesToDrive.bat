REM * This file is part of FrontLine.
REM * FrontLine is free software: you can redistribute it and/or modify
REM * it under the terms of the GNU General Public License as published by
REM * the Free Software Foundation, either version 3 of the License, or
REM * (at your option) any later version.
REM * FrontLine is distributed in the hope that it will be useful,
REM * but WITHOUT ANY WARRANTY; without even the implied warranty of
REM * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
REM * GNU General Public License for more details.
REM * You should have received a copy of the GNU General Public License
REM * along with FrontLine.  If not, see <https://www.gnu.org/licenses/>.

set /p number=Enter User Space Number: 
set /p drive=Enter Drive Letter:

robocopy .\spaces\%number%\Desktop %drive%\Desktop\ /e
robocopy .\spaces\%number%\Documents %drive%\Documents\ /e
robocopy .\spaces\%number%\Downloads %drive%\Downloads\ /e
robocopy .\spaces\%number%\Music %drive%\Music\ /e
robocopy .\spaces\%number%\Pictures %drive%\Pictures\ /e
robocopy .\spaces\%number%\Videos %drive%\Videos\ /e

PAUSE
