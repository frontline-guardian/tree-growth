REM * This file is part of FrontLine.
REM * FrontLine is free software: you can redistribute it and/or modify
REM * it under the terms of the GNU General Public License as published by
REM * the Free Software Foundation, either version 3 of the License, or
REM * (at your option) any later version.
REM * FrontLine is distributed in the hope that it will be useful,
REM * but WITHOUT ANY WARRANTY; without even the implied warranty of
REM * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
REM * GNU General Public License for more details.
REM * You should have received a copy of the GNU General Public License
REM * along with FrontLine.  If not, see <https://www.gnu.org/licenses/>.

rd /s /q build
rd /s /q dist
del binary\tree_growth.zip
pyinstaller --add-data env/;env  tree_growth.py
mkdir dist\tree_growth\spaces
mkdir dist\tree_growth\repository
powershell Compress-Archive -Path dist\tree_growth -DestinationPath binary\tree_growth.zip
